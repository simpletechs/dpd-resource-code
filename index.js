var Resource = require('deployd/lib/resource')
  , Script = require('deployd/lib/script')
  , util = require('util')
  , path = require('path')
  , fs = require('fs')
  , _nameSingleton
  , finalError = false;

function CodeResource(resourceName, options) {
  // in order for this resource to work, 
  // it is mandantory that there is only a single instance
  // (mainly because deployd only allows for a single list of events across all instances of one resource type)
  if(!_nameSingleton) {
    _nameSingleton = resourceName;
  } else if(_nameSingleton != resourceName) {
    finalError = true;
  }

  if(!finalError) {
    this.constructor.events = findEvents.call(this, options.configPath);
  }

  Resource.apply(this, arguments);
}
util.inherits(CodeResource, Resource);

module.exports = CodeResource;
module.exports.label = "Run Code";
module.exports.defaultPath = '/code';
module.exports.events = [];

module.exports.dashboard = {
    path: path.join(__dirname, 'dashboard')
  , pages: ['Code']
};

function findEvents(configPath) {
  var files = fs.readdirSync(configPath), fileList = [];
  for (var i in files) {
    if (!files.hasOwnProperty(i)) continue;

    var name = configPath + '/' + files[i];
    if (fs.statSync(name).isDirectory()) {
        continue;
    } else {
        if(name.substr(-3, 3) === '.js') {
          fileList.push(files[i].slice(0, -3));
        }
    }
  }

  return fileList;
}

function makeConsoleFn(type, messages) {
    return function() {
      var args = [].slice.call(arguments, 0);

      // remove automtically added callback
      if(typeof args[args.length-1] === 'function') {
        args.pop();
      }
      args.unshift(type);
      args.unshift((new Date()).toTimeString().split(' ')[0]); // add a timestamp

      var msg = args.map(function(itm) {
        return typeof itm === 'string'? itm : util.inspect(itm);
      }).join(' ');
      // console.log('msg: "', msg, '"');
      messages.push(msg);
    }
  }

function runCode(ctx, next) {
  var result, messages = [];

  var domain = {
    Promise: require('when'),

    internal: false,

    setResult: function(res) {
      result = res;
    },
    setTimeout: setTimeout,
    _console: { 
      log: makeConsoleFn('LOG', messages),
      info: makeConsoleFn('IFO', messages),
      error: makeConsoleFn('ERR', messages),
      warn: makeConsoleFn('WRN', messages),
      debug: makeConsoleFn('DBG', messages)
    }
  }

  var sendResponse = function(err) {
    // we don't want to respond with an error, even if we catched one
    // this is, so that we can actually log the error back to the user
    var resp = {
      messages: messages.join('\n')
    }

    if(typeof result !== 'undefined') {
      resp.result = typeof result === 'string'? result : util.inspect(result);
    }

    if(err) {
      resp.err = util.inspect(err);
      if(err.stack) {
        resp.err += '\n\nStack Trace:\n' + err.stack;
      }
    }
    ctx.done(null, resp);
  }

  try {
    this.events[ctx.body.activeEvent].run(ctx, domain, function(err) {
      sendResponse(err);
    });
  } catch(ex) {
    console.log('Got error:', ex, util.inspect(ex))
    sendResponse(ex);
  }
}

module.exports.prototype.handle = function (ctx, next) {
  if(finalError) {
    return ctx.done(null, {
      err: 'You can not have more than a single CodeResource active per deployd instance. Delete the new instance and restart deployd.'
    });
  }
  
  if(ctx.url === '/runCode') {
    // run the code...
    return runCode.call(this, ctx, next);
  } else if(ctx.url === '/createCode') {
    // create a new code file...
    var filename = path.join(this.options.configPath, ctx.body.newName + '.js');
    if(!fs.existsSync(filename)) {
      fs.writeFileSync(filename, "var console = _console;\n");
      ctx.done(null, {success: true});
    } else {
      ctx.done({success: false, message: 'File exists'}, null);
    }
  } else if(ctx.url === '/deleteCode') {
    // delete a code file...
    var filename = path.join(this.options.configPath, ctx.body.activeEvent + '.js');
    if(fs.existsSync(filename)) {
      fs.unlinkSync(filename);
      ctx.done(null, {success: true});
    } else {
      ctx.done({success: false}, null);
    }
  }
};
