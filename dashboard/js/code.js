(function() {
    var $consoleOutput = $('#console-output'),
        $errorOutput = $('#error-output'),
        $resultOutput = $('#result-output')

    function clearConsole() {
        $consoleOutput.text('');
        $errorOutput.text('');
        $resultOutput.text('');
    }

    function runCode() {
        var activeEvent = $('#event-nav .active a').attr('data-editor');
        dpd(Context.resourceId).exec('runCode', '', {activeEvent: activeEvent})
            .then(function(result) { 
                $consoleOutput.text(result.messages);
                $errorOutput.text(result.err || 'No Errors');
                $resultOutput.text(result.result || 'No Result');
            }, function(err) { 
                $consoleOutput.text(' - Error - ');
                $errorOutput.text(err);
                $resultOutput.text(' - Error - ');
            });
    }

    function createCode() {
        var newName = prompt('Enter a name for the new file');
        if(newName) {
            newName = newName.toLowerCase().replace(/[^a-z0-9]/g, '-');
            dpd(Context.resourceId).exec('createCode', '', {newName: newName})
                .then(function(result){
                    console.log('success', arguments);
                    window.location.hash = newName + '-panel';
                    window.location.reload();
                }, function() {
                    console.log('fail', arguments);
                    ui.error("Creating the file failed - try another name.").hide(2500).effect('slide');
                });
        }
        return false;
    }

    function deleteCode() {
        var activeEvent = $('#event-nav .active a').attr('data-editor');

        if(confirm('Are you sure you want to delete this code?')) {
            dpd(Context.resourceId).exec('deleteCode', '', {activeEvent: activeEvent})
                .then(function(result){
                    console.log('success', arguments);
                    window.location.href='.';
                }, function() {
                    console.log('fail', arguments);
                    ui.error("Deleting the file failed.").hide(2500).effect('slide');
                });
        }

        return false;
    }

    $(document).ready(function() {
        $('#btn-run-code').click(runCode);
        $('#btn-clear-console').click(clearConsole);


        $('#btn-create-code').click(createCode);
        $('#btn-delete-code').click(deleteCode);

        // move it the the navbar
        ($('<li/>').append($('#btn-create-code'))).appendTo($('#event-nav'));
    })
})();